# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.1.0] - 2019-10-16

### Added

- Added prop 'layoutSize' to define size to each container
- Added prop 'useHeader' to remove the padding top used by default
- Added Top component to show before the form
- Added Actions component to show after the form and before of the footer
- Added prop 'title' to show a title before of the form

### Fixed

- Fixed typings

## [1.0.1] - 2019-09-26

### Fixed

- Fixed dependencies version

## [1.0.0] - 2019-05-16

### Release

### Added

- Added docs api (html, md)

### Fixed

- Fixed logo images by default

# Unreleased

## [1.0.0-beta.0] - 2019-05-15

### Added

- Upload and publish first version

[1.1.0]: https://bitbucket.org/ticmakers/rn-recovery-password/src/v1.1.0/
[1.0.1]: https://bitbucket.org/ticmakers/rn-recovery-password/src/v1.0.1/
[1.0.0]: https://bitbucket.org/ticmakers/rn-recovery-password/src/v1.0.0/
