[@ticmakers-react-native/recovery-password](../README.md) > ["index.d"](../modules/_index_d_.md) > [IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)

# Interface: IRecoveryPasswordState

Interface to define the props of the RecoveryPassword component

*__export__*: 

*__interface__*: IRecoveryPasswordState

## Hierarchy

**IRecoveryPasswordState**

## Index

---

