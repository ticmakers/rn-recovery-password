[@ticmakers-react-native/recovery-password](../README.md) > ["index.d"](../modules/_index_d_.md) > [IRecoveryPasswordModelProps](../interfaces/_index_d_.irecoverypasswordmodelprops.md)

# Interface: IRecoveryPasswordModelProps

Interface to define the props of the email

*__interface__*: IRecoveryPasswordModelProps

## Hierarchy

**IRecoveryPasswordModelProps**

## Index

### Properties

* [icon](_index_d_.irecoverypasswordmodelprops.md#icon)
* [label](_index_d_.irecoverypasswordmodelprops.md#label)
* [rules](_index_d_.irecoverypasswordmodelprops.md#rules)
* [value](_index_d_.irecoverypasswordmodelprops.md#value)

---

## Properties

<a id="icon"></a>

### `<Optional>` icon

**● icon**: *`IIconProps` \| `TypeComponent`*

*Defined in index.d.ts:57*

Icon props or React-Native component to show a icon in the Input

*__type__*: {(IIconProps \| TypeComponent)}

___
<a id="label"></a>

### `<Optional>` label

**● label**: *`undefined` \| `string`*

*Defined in index.d.ts:45*

Define the label of a Input

*__type__*: {string}

___
<a id="rules"></a>

### `<Optional>` rules

**● rules**: *`IInputRules`*

*Defined in index.d.ts:51*

Object with the rules for the Input

*__type__*: {IInputRules}

___
<a id="value"></a>

### `<Optional>` value

**● value**: *`any`*

*Defined in index.d.ts:63*

Set a value to the Input

*__type__*: {\*}

___

