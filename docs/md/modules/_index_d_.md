[@ticmakers-react-native/recovery-password](../README.md) > ["index.d"](../modules/_index_d_.md)

# External module: "index.d"

## Index

### Modules

* ["@ticmakers-react-native/recovery-password"](_index_d_.__ticmakers_react_native_recovery_password_.md)

### Classes

* [RecoveryPassword](../classes/_index_d_.recoverypassword.md)

### Interfaces

* [IRecoveryPasswordModelProps](../interfaces/_index_d_.irecoverypasswordmodelprops.md)
* [IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)
* [IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)

### Type aliases

* [TypeRecoveryPasswordLang](_index_d_.md#typerecoverypasswordlang)
* [TypeRecoveryPasswordLayoutSize](_index_d_.md#typerecoverypasswordlayoutsize)
* [TypeRecoveryPasswordSource](_index_d_.md#typerecoverypasswordsource)

---

## Type aliases

<a id="typerecoverypasswordlang"></a>

###  TypeRecoveryPasswordLang

**Ƭ TypeRecoveryPasswordLang**: *"es" \| "en" \| "fr" \| "fa"*

*Defined in index.d.ts:23*

Type to define the language available

___
<a id="typerecoverypasswordlayoutsize"></a>

###  TypeRecoveryPasswordLayoutSize

**Ƭ TypeRecoveryPasswordLayoutSize**: *`object`*

*Defined in index.d.ts:28*

Type to define the layout size

#### Type declaration

`Optional`  actions: `undefined` \| `number`

`Optional`  footer: `undefined` \| `number`

`Optional`  form: `undefined` \| `number`

`Optional`  logo: `undefined` \| `number`

`Optional`  top: `undefined` \| `number`

___
<a id="typerecoverypasswordsource"></a>

###  TypeRecoveryPasswordSource

**Ƭ TypeRecoveryPasswordSource**: *`ImageSourcePropType`*

*Defined in index.d.ts:18*

Type to define the prop source of the RecoveryPassword component

___

