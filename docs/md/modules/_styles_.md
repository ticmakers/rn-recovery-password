[@ticmakers-react-native/recovery-password](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [logoSize](_styles_.md#logosize)
* [styles](_styles_.md#styles)

---

## Variables

<a id="logosize"></a>

### `<Const>` logoSize

**● logoSize**: *`90`* = 90

*Defined in styles.ts:4*

___
<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  btn: {
    marginBottom: 8,
  },

  wrapper: {
    paddingTop: 50,
  },

  container: {
  },

  logoContainer: {
    justifyContent: 'center',
  },

  logoImageContainer: {
  },

  logoImage: {
    height: logoSize,
    resizeMode: 'contain',
    // width: '100%',
  },

  topContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    paddingVertical: 16,
  },

  topTitle: {
    alignItems: 'center',
    alignSelf: 'center',
    fontSize: 20,
  },

  formContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  formInputContainer: {
    width: '90%',
  },

  formSubmit: {
    marginTop: 16,
  },

  actionsContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    paddingVertical: 8,
  },

  footerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  boxShadow: {
    // marginHorizontal: 8,
    backgroundColor: 'transparent',
    elevation: 5,
    shadowColor: '#FAE057',
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: '100%',
  },
})

*Defined in styles.ts:6*

#### Type declaration

 container: `object`

 logoImageContainer: `object`

 actionsContainer: `object`

 flexDirection: "column"

 justifyContent: "center"

 paddingVertical: `number`

 boxShadow: `object`

 backgroundColor: `string`

 elevation: `number`

 shadowColor: `string`

 shadowOpacity: `number`

 shadowRadius: `number`

 width: `string`

 shadowOffset: `object`

 height: `number`

 width: `number`

 btn: `object`

 marginBottom: `number`

 footerContainer: `object`

 alignItems: "center"

 justifyContent: "center"

 formContainer: `object`

 alignItems: "center"

 justifyContent: "center"

 formInputContainer: `object`

 width: `string`

 formSubmit: `object`

 marginTop: `number`

 logoContainer: `object`

 justifyContent: "center"

 logoImage: `object`

 height: `number`

 resizeMode: "contain"

 topContainer: `object`

 flexDirection: "column"

 justifyContent: "center"

 paddingVertical: `number`

 topTitle: `object`

 alignItems: "center"

 alignSelf: "center"

 fontSize: `number`

 wrapper: `object`

 paddingTop: `number`

___

