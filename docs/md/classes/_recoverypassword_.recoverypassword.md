[@ticmakers-react-native/recovery-password](../README.md) > ["RecoveryPassword"](../modules/_recoverypassword_.md) > [RecoveryPassword](../classes/_recoverypassword_.recoverypassword.md)

# Class: RecoveryPassword

Class to define the RecoveryPassword component

*__class__*: RecoveryPassword

*__extends__*: {React.Component<IRecoveryPasswordProps, IRecoveryPasswordState>}

## Type parameters
#### SS 
## Hierarchy

 `Component`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md), [IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>

**↳ RecoveryPassword**

## Index

### Constructors

* [constructor](_recoverypassword_.recoverypassword.md#constructor)

### Properties

* [context](_recoverypassword_.recoverypassword.md#context)
* [emailInput](_recoverypassword_.recoverypassword.md#emailinput)
* [props](_recoverypassword_.recoverypassword.md#props)
* [refs](_recoverypassword_.recoverypassword.md#refs)
* [state](_recoverypassword_.recoverypassword.md#state)
* [contextType](_recoverypassword_.recoverypassword.md#contexttype)

### Methods

* [Actions](_recoverypassword_.recoverypassword.md#actions)
* [Footer](_recoverypassword_.recoverypassword.md#footer)
* [Form](_recoverypassword_.recoverypassword.md#form)
* [Logo](_recoverypassword_.recoverypassword.md#logo)
* [SubmitButton](_recoverypassword_.recoverypassword.md#submitbutton)
* [Top](_recoverypassword_.recoverypassword.md#top)
* [UNSAFE_componentWillMount](_recoverypassword_.recoverypassword.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_recoverypassword_.recoverypassword.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_recoverypassword_.recoverypassword.md#unsafe_componentwillupdate)
* [_onBack](_recoverypassword_.recoverypassword.md#_onback)
* [_onSubmit](_recoverypassword_.recoverypassword.md#_onsubmit)
* [_processProps](_recoverypassword_.recoverypassword.md#_processprops)
* [componentDidCatch](_recoverypassword_.recoverypassword.md#componentdidcatch)
* [componentDidMount](_recoverypassword_.recoverypassword.md#componentdidmount)
* [componentDidUpdate](_recoverypassword_.recoverypassword.md#componentdidupdate)
* [componentWillMount](_recoverypassword_.recoverypassword.md#componentwillmount)
* [componentWillReceiveProps](_recoverypassword_.recoverypassword.md#componentwillreceiveprops)
* [componentWillUnmount](_recoverypassword_.recoverypassword.md#componentwillunmount)
* [componentWillUpdate](_recoverypassword_.recoverypassword.md#componentwillupdate)
* [forceUpdate](_recoverypassword_.recoverypassword.md#forceupdate)
* [getSnapshotBeforeUpdate](_recoverypassword_.recoverypassword.md#getsnapshotbeforeupdate)
* [isValid](_recoverypassword_.recoverypassword.md#isvalid)
* [render](_recoverypassword_.recoverypassword.md#render)
* [setState](_recoverypassword_.recoverypassword.md#setstate)
* [shouldComponentUpdate](_recoverypassword_.recoverypassword.md#shouldcomponentupdate)

### Object literals

* [labels](_recoverypassword_.recoverypassword.md#labels)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new RecoveryPassword**(props: *[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)*): [RecoveryPassword](_recoverypassword_.recoverypassword.md)

*Overrides Component.__constructor*

*Defined in RecoveryPassword.tsx:36*

Creates an instance of RecoveryPassword.

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | [IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md) |  An object of RecoveryPasswordProps |

**Returns:** [RecoveryPassword](_recoverypassword_.recoverypassword.md)

___

## Properties

<a id="context"></a>

###  context

**● context**: *`any`*

*Inherited from Component.context*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:425*

If using the new style context, re-declare this in your class to be the `React.ContextType` of your `static contextType`.

```ts
static contextType = MyContext
context!: React.ContextType<typeof MyContext>
```

*__deprecated__*: if used without a type annotation, or without static contextType

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

___
<a id="emailinput"></a>

###  emailInput

**● emailInput**: *`Input` \| `null` \| `undefined`*

*Defined in RecoveryPassword.tsx:24*

Reference of email input

*__type__*: {(Input \| null \| undefined)}

___
<a id="props"></a>

###  props

**● props**: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> & `Readonly`<`object`>*

*Inherited from Component.props*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:450*

___
<a id="refs"></a>

###  refs

**● refs**: *`object`*

*Inherited from Component.refs*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:456*

*__deprecated__*: [https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs](https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs)

#### Type declaration

[key: `string`]: `ReactInstance`

___
<a id="state"></a>

###  state

**● state**: *`Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>*

*Inherited from Component.state*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:451*

___
<a id="contexttype"></a>

### `<Static>``<Optional>` contextType

**● contextType**: *`Context`<`any`>*

*Inherited from Component.contextType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:410*

If set, `this.context` will be set at runtime to the current value of the given Context.

Usage:

```ts
type MyContext = number
const Ctx = React.createContext<MyContext>(0)

class Foo extends React.Component {
  static contextType = Ctx
  context!: React.ContextType<typeof Ctx>
  render () {
    return <>My context's value: {this.context}</>;
  }
}
```

*__see__*: [https://reactjs.org/docs/context.html#classcontexttype](https://reactjs.org/docs/context.html#classcontexttype)

___

## Methods

<a id="actions"></a>

###  Actions

▸ **Actions**(): `TypeComponent`

*Defined in RecoveryPassword.tsx:208*

Method that renders the Actions component

**Returns:** `TypeComponent`

___
<a id="footer"></a>

###  Footer

▸ **Footer**(): `TypeComponent`

*Defined in RecoveryPassword.tsx:252*

Method that renders the Footer component

**Returns:** `TypeComponent`

___
<a id="form"></a>

###  Form

▸ **Form**(): `TypeComponent`

*Defined in RecoveryPassword.tsx:172*

Method that renders the Form component

**Returns:** `TypeComponent`

___
<a id="logo"></a>

###  Logo

▸ **Logo**(): `TypeComponent`

*Defined in RecoveryPassword.tsx:99*

Method that renders the Logo component

**Returns:** `TypeComponent`

___
<a id="submitbutton"></a>

###  SubmitButton

▸ **SubmitButton**(): `TypeComponent`

*Defined in RecoveryPassword.tsx:228*

Method that renders the SubmitButton component

**Returns:** `TypeComponent`

___
<a id="top"></a>

###  Top

▸ **Top**(): `TypeComponent`

*Defined in RecoveryPassword.tsx:132*

Method that renders the Top component

**Returns:** `TypeComponent`

___
<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:638*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:670*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, nextState: *`Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:698*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| nextState | `Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="_onback"></a>

### `<Private>` _onBack

▸ **_onBack**(): `void`

*Defined in RecoveryPassword.tsx:312*

Method that fire when the back button is pressed

**Returns:** `void`

___
<a id="_onsubmit"></a>

### `<Private>` _onSubmit

▸ **_onSubmit**(): `void`

*Defined in RecoveryPassword.tsx:296*

Method that fire when the submit button is pressed

**Returns:** `void`

___
<a id="_processprops"></a>

### `<Private>` _processProps

▸ **_processProps**(): [IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)

*Defined in RecoveryPassword.tsx:325*

Method to process the props

**Returns:** [IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:567*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:546*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, prevState: *`Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:609*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| prevState | `Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:624*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:653*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:562*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, nextState: *`Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:683*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| nextState | `Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="forceupdate"></a>

###  forceUpdate

▸ **forceUpdate**(callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.forceUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:442*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, prevState: *`Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:603*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| prevState | `Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)> |

**Returns:** `SS` \| `null`

___
<a id="isvalid"></a>

###  isValid

▸ **isValid**(): `boolean`

*Defined in RecoveryPassword.tsx:284*

Method to check if the inputs are valid

**Returns:** `boolean`

___
<a id="render"></a>

###  render

▸ **render**(): `TypeComponent`

*Overrides Component.render*

*Defined in RecoveryPassword.tsx:51*

Method that renders the component

**Returns:** `TypeComponent`

___
<a id="setstate"></a>

###  setState

▸ **setState**<`K`>(state: *`function` \| `null` \| `S` \| `object`*, callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.setState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:437*

**Type parameters:**

#### K :  `keyof IRecoveryPasswordState`
**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `function` \| `null` \| `S` \| `object` |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)>*, nextState: *`Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/RecoveryPassword/node_modules/@types/react/index.d.ts:557*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[IRecoveryPasswordProps](../interfaces/_index_d_.irecoverypasswordprops.md)> |
| nextState | `Readonly`<[IRecoveryPasswordState](../interfaces/_index_d_.irecoverypasswordstate.md)> |
| nextContext | `any` |

**Returns:** `boolean`

___

## Object literals

<a id="labels"></a>

###  labels

**labels**: *`object`*

*Defined in RecoveryPassword.tsx:29*

Default locale labels

<a id="labels.en"></a>

####  en

**en**: *`object`*

*Defined in RecoveryPassword.tsx:30*

<a id="labels.en.email"></a>

####  email

**● email**: *`string`* = "Email"

*Defined in RecoveryPassword.tsx:31*

___

___
<a id="labels.es"></a>

####  es

**es**: *`object`*

*Defined in RecoveryPassword.tsx:33*

<a id="labels.es.email-1"></a>

####  email

**● email**: *`string`* = "Correo electrónico"

*Defined in RecoveryPassword.tsx:34*

___

___

___

