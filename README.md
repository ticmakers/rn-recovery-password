# TIC Makers - React Native Recovery Password
React native component for recovery password.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Recovery Password Expo's snack]()

## Install

Install `@ticmakers-react-native/recovery-password` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/recovery-password --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/recovery-password
```

## How to use?

```javascript
import React from 'react'
import RecoveryPassword from '@ticmakers-react-native/recovery-password'

export default class App extends React.Component {

  render() {
    const  emailProps: IRecoveryPasswordModelProps = {
      icon: {
        color: '#76808b',
        name: Platform.select({ android: 'email', ios: 'ios-mail' }),
        type: Platform.select({ android: 'material-community', ios: 'ionicon' }),
      } as IIconProps,
      rules: { required: true, email: true, minLength: 6, maxLength: 100 },
    }

    return (
      <RecoveryPassword
        lang="es"
        ref={ comp => this.recoveryPassword = comp }
        emailProps={ emailProps }
        containerStyle={ styles.formContainer }
        logoSource={ require('./../../assets/images/icon.png') }
        logoSize={ 120 }
        submitLabel="Recuperar contraseña"
        submitStyle={ styles.formSubmit }
        FooterComponent={ this.BackButton() }
        onSubmit={() => this.onSubmit()}
      />
    )
  }
}
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| ---- | ---- | ---- | ----

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Create tests
- Add new props and components in readme
- Improve README

## Version 1.1.0 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-recovery-password/src/master/CHANGELOG.md
