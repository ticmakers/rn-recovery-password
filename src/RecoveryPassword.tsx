import * as React from 'react'
import { StyleSheet, Platform, Text } from 'react-native'
import tinycolor from 'tinycolor2'

import { AppHelper, TypeComponent } from '@ticmakers-react-native/core'
import { Col, Grid, Row } from '@ticmakers-react-native/flexbox'
import Button from '@ticmakers-react-native/button'
import Image, { IImageProps } from '@ticmakers-react-native/image'
import { IIconProps, TypeIcons } from '@ticmakers-react-native/icon'
import Input, { IInputProps } from '@ticmakers-react-native/input'
import { IRecoveryPasswordProps, IRecoveryPasswordState } from './../index'
import styles from './styles'

/**
 * Class to define the RecoveryPassword component
 * @class RecoveryPassword
 * @extends {React.Component<IRecoveryPasswordProps, IRecoveryPasswordState>}
 */
export default class RecoveryPassword extends React.Component<IRecoveryPasswordProps, IRecoveryPasswordState> {
  /**
   * Reference of email input
   * @type {(Input | null | undefined)}
   */
  public emailInput: Input | null | undefined

  /**
   * Default locale labels
   */
  public labels = {
    en: {
      email: 'Email',
    },
    es: {
      email: 'Correo electrónico',
    },
  }

  /**
   * Creates an instance of RecoveryPassword.
   * @param {IRecoveryPasswordProps} props    An object of RecoveryPasswordProps
   */
  constructor(props: IRecoveryPasswordProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent {
    const { containerStyle, layoutSize, shadowBox, useHeader } = this._processProps()

    const containerProps = {
      containerStyle: StyleSheet.flatten([styles.container,  shadowBox && styles.boxShadow, containerStyle]),
      style: StyleSheet.flatten([!useHeader && styles.wrapper]),
    }

    const logoContainerSize = (layoutSize && typeof layoutSize.logo !== 'undefined' ? layoutSize.logo : 2)
    const topContainerSize = (layoutSize && typeof layoutSize.top !== 'undefined' ? layoutSize.top : 1)
    const formContainerSize = (layoutSize && typeof layoutSize.form !== 'undefined' ? layoutSize.form : 3)
    const actionsContainerSize = (layoutSize && typeof layoutSize.actions !== 'undefined' ? layoutSize.actions : 1)
    const footerContainerSize = (layoutSize && typeof layoutSize.footer !== 'undefined' ? layoutSize.footer : 1)

    return (
      <Grid { ...containerProps }>
        {/** Logo Container */}
        <Row size={ logoContainerSize }>
          { this.Logo() }
        </Row>

        {/** Top Container */}
        <Row size={ topContainerSize }>
          { this.Top() }
        </Row>

        {/** Form Container */}
        <Row size={ formContainerSize }>
          { this.Form() }
        </Row>

        {/** Actions Container */}
        <Row size={ actionsContainerSize }>
          { this.Actions() }
        </Row>

        {/** Footer Container */}
        <Row size={ footerContainerSize }>
          { this.Footer() }
        </Row>
      </Grid>
    )
  }

  /**
   * Method that renders the Logo component
   * @returns {TypeComponent}
   */
  public Logo(): TypeComponent {
    const { LogoComponent, hideLogo, logoContainerStyle, logoSize, logoSource } = this._processProps()
    const containerProps = {
      style: StyleSheet.flatten([styles.logoContainer, logoContainerStyle]),
    }

    const imageProps = {
      containerStyle: StyleSheet.flatten([styles.logoImageContainer]),
      source: logoSource,
      style: StyleSheet.flatten([styles.logoImage, logoSize && { height: logoSize } as any]),
    }

    if (!hideLogo) {
      if (AppHelper.isComponent(LogoComponent)) {
        return (
          <Col { ...containerProps } direction="row">
            { LogoComponent }
          </Col>
        )
      }

      return (
        <Col { ...containerProps }>
          <Image { ...imageProps as IImageProps } />
        </Col>
      )
    }
  }

  /**
   * Method that renders the Top component
   * @returns {TypeComponent}
   */
  public Top(): TypeComponent {
    const { TopComponent, hideTop, topContainerStyle, title, titleStyle } = this._processProps()

    const containerProps = {
      style: StyleSheet.flatten([styles.topContainer, topContainerStyle]),
    }

    const titleProps = {
      style: StyleSheet.flatten([styles.topTitle, titleStyle]),
    }

    if (!hideTop) {
      let titleComponent
      let topComponent

      if (title) {
        if (AppHelper.isComponent(title) || AppHelper.isElement(title)) {
          titleComponent = React.cloneElement(title as any, titleProps)
        } else {
          titleComponent = <Text { ...titleProps }>title</Text>
        }
      }

      if (TopComponent && (AppHelper.isComponent(TopComponent) || AppHelper.isElement(TopComponent))) {
        topComponent = React.cloneElement(TopComponent as any)
      }

      return (
        <Col { ...containerProps }>
          { titleComponent }
          { topComponent }
        </Col>
      )
    }
  }

  /**
   * Method that renders the Form component
   * @returns {TypeComponent}
   */
  public Form(): TypeComponent {
    const { emailProps, hideForm, hideSubmit, hideValidationIcons, lang } = this._processProps()
    const _lang = lang || 'en'
    const defaultEmailLabel = (this.labels as any)[_lang] && (this.labels as any)[_lang].email || 'Email'
    const emailIconName = Platform.select({ android: 'email', ios: 'ios-mail' })
    const emailIconType = Platform.select({ android: 'material-community', ios: 'ionicon' })
    const emailIcon: IIconProps = { name: emailIconName, type: emailIconType as TypeIcons }

    const inputEmailProps: IInputProps = {
      hideValidationIcons,
      // tslint:disable-next-line: object-literal-sort-keys
      containerStyle: StyleSheet.flatten([styles.formInputContainer]),
      iconLeft: (emailProps && emailProps.icon) || emailIcon,
      label: (emailProps && emailProps.label) || defaultEmailLabel,
      lang: _lang,
      rules: (emailProps && emailProps.rules) || { required: true, email: true },
    }

    if (!hideForm) {
      return (
        <Col style={ [styles.formContainer] }>
          <Input
            ref={ c => this.emailInput = c }
            { ...inputEmailProps }
          />

          {!hideSubmit && this.SubmitButton() }
        </Col>
      )
    }
  }

  /**
   * Method that renders the Actions component
   * @returns {TypeComponent}
   */
  public Actions(): TypeComponent {
    const { ActionsComponent, hideActions, actionsContainerStyle } = this._processProps()

    const containerProps = {
      style: StyleSheet.flatten([styles.actionsContainer, actionsContainerStyle]),
    }

    if (!hideActions) {
      if (ActionsComponent && (AppHelper.isComponent(ActionsComponent) || AppHelper.isElement(ActionsComponent))) {
        return React.cloneElement(ActionsComponent as any, containerProps)
      }

      return <Col { ...containerProps } />
    }
  }

  /**
   * Method that renders the SubmitButton component
   * @returns {TypeComponent}
   */
  public SubmitButton(): TypeComponent {
    const { SubmitComponent, hideSubmit, lang, submitLabel, submitStyle } = this._processProps()
    const _lang = lang || 'en'

    const buttonProps = {
      block: true,
      onPress: () => this._onSubmit(),
      style: StyleSheet.flatten([styles.btn, styles.formSubmit, submitStyle]),
      title: submitLabel ? submitLabel : _lang === 'es' ? 'Enviar' : 'Send',
    }

    if (!hideSubmit) {
      if (AppHelper.isComponent(SubmitComponent)) {
        return React.cloneElement(SubmitComponent as any)
      }

      return <Button { ...buttonProps } />
    }
  }

  /**
   * Method that renders the Footer component
   * @returns {TypeComponent}
   */
  public Footer(): TypeComponent {
    const { FooterComponent, hideFooter, lang } = this._processProps()
    const _lang = lang || 'en'

    const footerButtonProps = {
      center: true,
      link: true,
      onPress: () => this._onBack(),
      title: _lang === 'es' ? 'Volver' : 'Back',
    }

    if (!hideFooter) {
      if (AppHelper.isComponent(FooterComponent)) {
        return (
          <Col style={ [styles.footerContainer] } direction="row">
            { FooterComponent }
          </Col>
        )
      }

      return (
        <Col style={ [styles.footerContainer] } direction="row">
          <Button { ...footerButtonProps } />
        </Col>
      )
    }
  }

  /**
   * Method to check if the inputs are valid
   * @returns {boolean}
   */
  public isValid(): boolean {
    const emailValue = this.emailInput && this.emailInput.state.value
    const emailIsValid = this.emailInput && this.emailInput.isValid()

    return (!!emailValue && !!emailIsValid)
  }

  /**
   * Method that fire when the submit button is pressed
   * @private
   * @returns {void}
   */
  private _onSubmit(): void {
    const { onSubmit } = this._processProps()
    // const data = {
    //   email:
    // }

    if (onSubmit) {
      return onSubmit()
    }
  }

  /**
   * Method that fire when the back button is pressed
   * @private
   * @returns {void}
   */
  private _onBack(): void {
    const { onBack } = this._processProps()

    if (onBack) {
      return onBack()
    }
  }

  /**
   * Method to process the props
   * @private
   * @returns {IRecoveryPasswordProps}
   */
  private _processProps(): IRecoveryPasswordProps {
    const { ActionsComponent, FooterComponent, LogoComponent, SubmitComponent, TopComponent, actionsContainerStyle, containerStyle, emailProps, hideActions, hideFooter, hideForm, hideLogo, hideSubmit, hideTop, hideValidationIcons, lang, layoutSize, logoContainerStyle, logoSize, logoSource, onBack, onSubmit, shadowBox, submitLabel, submitStyle, title, titleStyle, topContainerStyle, useHeader } = this.props

    const props: IRecoveryPasswordProps = {
      ActionsComponent: (typeof ActionsComponent !== 'undefined' ? ActionsComponent : undefined),
      FooterComponent: (typeof FooterComponent !== 'undefined' ? FooterComponent : undefined),
      LogoComponent: (typeof LogoComponent !== 'undefined' ? LogoComponent : undefined),
      SubmitComponent: (typeof SubmitComponent !== 'undefined' ? SubmitComponent : undefined),
      TopComponent: (typeof TopComponent !== 'undefined' ? TopComponent : undefined),
      actionsContainerStyle: (typeof actionsContainerStyle !== 'undefined' ? actionsContainerStyle : undefined),
      containerStyle: (typeof containerStyle !== 'undefined' ? containerStyle : undefined),
      emailProps: (typeof emailProps !== 'undefined' ? emailProps : undefined),
      hideActions: (typeof hideActions !== 'undefined' ? hideActions : false),
      hideFooter: (typeof hideFooter !== 'undefined' ? hideFooter : false),
      hideForm: (typeof hideForm !== 'undefined' ? hideForm : false),
      hideLogo: (typeof hideLogo !== 'undefined' ? hideLogo : false),
      hideSubmit: (typeof hideSubmit !== 'undefined' ? hideSubmit : false),
      hideTop: (typeof hideTop !== 'undefined' ? hideTop : false),
      hideValidationIcons: (typeof hideValidationIcons !== 'undefined' ? hideValidationIcons : false),
      lang: (typeof lang !== 'undefined' ? lang : 'en'),
      layoutSize: (typeof layoutSize !== 'undefined' ? layoutSize : undefined),
      logoContainerStyle: (typeof logoContainerStyle !== 'undefined' ? logoContainerStyle : undefined),
      logoSize: (typeof logoSize !== 'undefined' ? logoSize : undefined),
      logoSource: (typeof logoSource !== 'undefined' ? logoSource : require('./../assets/icon.png')),
      onBack: (typeof onBack !== 'undefined' ? onBack : undefined),
      onSubmit: (typeof onSubmit !== 'undefined' ? onSubmit : undefined),
      shadowBox: (typeof shadowBox !== 'undefined' ? shadowBox : false),
      submitLabel: (typeof submitLabel !== 'undefined' ? submitLabel : undefined),
      submitStyle: (typeof submitStyle !== 'undefined' ? submitStyle : undefined),
      title: (typeof title !== 'undefined' ? title : undefined),
      titleStyle: (typeof titleStyle !== 'undefined' ? titleStyle : undefined),
      topContainerStyle: (typeof topContainerStyle !== 'undefined' ? topContainerStyle : undefined),
      useHeader: (typeof useHeader !== 'undefined' ? useHeader : false),
    }

    return props
  }
}
