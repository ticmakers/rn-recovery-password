import * as React from 'react'
import {
  ImageSourcePropType,
  RecursiveArray,
  RegisteredStyle,
  StyleProp,
  ViewProps,
  ViewStyle,
} from 'react-native'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'
import { IIconProps } from '@ticmakers-react-native/icon'
import Input, { IInputRules } from '@ticmakers-react-native/input'

/**
 * Type to define the prop source of the RecoveryPassword component
 */
export type TypeRecoveryPasswordSource = ImageSourcePropType

/**
 * Type to define the language available
 */
export type TypeRecoveryPasswordLang = 'es' | 'en' | 'fr' | 'fa'

/**
 * Type to define the layout size
 */
export type TypeRecoveryPasswordLayoutSize = {
  logo?: number
  top?: number
  form?: number
  actions?: number
  footer?: number
}

/**
 * Interface to define the props of the email
 * @interface IRecoveryPasswordModelProps
 */
export interface IRecoveryPasswordModelProps {
  /**
   * Define the label of a Input
   * @type {string}
   */
  label?: string

  /**
   * Object with the rules for the Input
   * @type {IInputRules}
   */
  rules?: IInputRules

  /**
   * Icon props or React-Native component to show a icon in the Input
   * @type {(IIconProps | TypeComponent)}
   */
  icon?: IIconProps | TypeComponent

  /**
   * Set a value to the Input
   * @type {*}
   */
  value?: any
}

/**
 * Interface to define the props of the RecoveryPassword component
 * @interface IRecoveryPasswordProps
 */
export interface IRecoveryPasswordProps extends ViewProps {
  /**
   * A React-Native component to replace the Actions component
   * @type {TypeComponent}
   */
  ActionsComponent?: TypeComponent

  /**
   * A React-Native component to replace the footer component
   * @type {TypeComponent}
   */
  FooterComponent?: TypeComponent

  /**
   * A React-Native component to replace the logo
   * @type {TypeComponent}
   */
  LogoComponent?: TypeComponent

  /**
   * A React-Native component to replace the button submit
   * @type {TypeComponent}
   */
  SubmitComponent?: TypeComponent

  /**
   * A React-Native component to replace the Top component
   * @type {TypeComponent}
   */
  TopComponent?: TypeComponent

  /**
   * Apply a custom style to the actions container
   * @type {TypeStyle}
   */
  actionsContainerStyle?: TypeStyle

  /**
   * Apply a custom style to the container
   * @type {TypeStyle}
   */
  containerStyle?: TypeStyle

  /**
   * Define the props for the input email
   * @type {IRecoveryPasswordModelProps}
   */
  emailProps?: IRecoveryPasswordModelProps

  /**
   * Set true to hide the actions component
   * @type {boolean}
   * @default false
   */
  hideActions?: boolean

  /**
   * Set true to hide the footer component
   * @type {boolean}
   * @default false
   */
  hideFooter?: boolean

  /**
   * Set true to hide the form component
   * @type {boolean}
   * @default false
   */
  hideForm?: boolean

  /**
   * Set true to hide the logo component
   * @type {boolean}
   * @default false
   */
  hideLogo?: boolean

  /**
   * Set true to hide the submit button
   * @type {boolean}
   * @default false
   */
  hideSubmit?: boolean

  /**
   * Set true to hide the top component
   * @type {boolean}
   * @default false
   */
  hideTop?: boolean

  /**
   * Set true to hide the icon validations
   * @type {boolean}
   * @default false
   */
  hideValidationIcons?: boolean

  /**
   * Define the language to show the messages
   * @type {TypeRecoveryPasswordLang}
   * @default en
   */
  lang?: TypeRecoveryPasswordLang

  /**
   * Set a custom size to the layout of the component
   * @type {TypeRecoveryPasswordLayoutSize}
   * @memberof IRecoveryPasswordProps
   */
  layoutSize?: TypeRecoveryPasswordLayoutSize

  /**
   * Apply a custom style to the logo container
   * @type {TypeStyle}
   */
  logoContainerStyle?: TypeStyle

  /**
   * A number to define the size of the logo
   * @type {number}
   */
  logoSize?: number

  /**
   * Source to define the image of the logo
   * @type {TypeRecoveryPasswordSource}
   */
  logoSource?: TypeRecoveryPasswordSource

  /**
   * Method that fire when the button back is pressed
   */
  onBack?: () => any

  /**
   * Method that fire when the button submit is pressed
   */
  onSubmit?: () => any

  /**
   * Set true to apply a shadow box to the container
   * @type {boolean}
   * @default false
   */
  shadowBox?: boolean

  /**
   * A String to define the label to the button submit
   * @type {string}
   */
  submitLabel?: string

  /**
   * Apply a custom style to the button submit
   * @type {TypeStyle}
   */
  submitStyle?: TypeStyle

  /**
   * Show a title before the form container
   * @type {string}
   */
  title?: TypeComponent | string

  /**
   * Apply a custom style to the top title
   * @type {TypeStyle}
   */
  titleStyle?: TypeStyle

  /**
   * Apply a custom style to the top container
   * @type {TypeStyle}
   */
  topContainerStyle?: TypeStyle

  /**
   * Set true if use a header component
   * @type {boolean}
   */
  useHeader?: boolean
}

/**
 * Interface to define the props of the RecoveryPassword component
 * @export
 * @interface IRecoveryPasswordState
 */
export interface IRecoveryPasswordState {
}

/**
 * Class to define the RecoveryPassword component
 * @class RecoveryPassword
 * @extends {React.Component<IRecoveryPasswordProps, IRecoveryPasswordState>}
 */
declare class RecoveryPassword extends React.Component<IRecoveryPasswordProps, IRecoveryPasswordState> {
  /**
   * Reference of email input
   * @type {(Input | undefined)}
   */
  public emailInput: Input | undefined

  /**
   * Default locale labels
   */
  public labels: {
    en: {
      email: string,
    },
    es: {
      email: string,
    },
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent

  /**
   * Method that renders the Logo component
   * @returns {TypeComponent}
   */
  public Logo(): TypeComponent

  /**
   * Method that renders the Top component
   * @returns {TypeComponent}
   */
  public Top(): TypeComponent

  /**
   * Method that renders the Form component
   * @returns {TypeComponent}
   */
  public Form(): TypeComponent

  /**
   * Method that renders the Actions component
   * @returns {TypeComponent}
   */
  public Actions(): TypeComponent

  /**
   * Method that renders the SubmitButton component
   * @returns {TypeComponent}
   */
  public SubmitButton(): TypeComponent

  /**
   * Method that renders the Footer component
   * @returns {TypeComponent}
   */
  public Footer(): TypeComponent

  /**
   * Method to check if the inputs are valid
   * @returns {boolean}
   */
  public isValid(): boolean

  /**
   * Method that fire when the submit button is pressed
   * @private
   * @returns {void}
   */
  private _onSubmit(): void

  /**
   * Method that fire when the back button is pressed
   * @private
   * @returns {void}
   */
  private _onBack(): void

  /**
   * Method to valid if a background color is light
   * @private
   * @param {string} backgroundColor    A string color rgb,rgba,hex,etc...
   * @returns {boolean}
   * @memberof Login
   */
  private _isLight(backgroundColor: string): boolean

  /**
   * Method to process the props
   * @private
   * @returns {IRecoveryPasswordState}
   */
  private _processProps(): IRecoveryPasswordState
}

declare module '@ticmakers-react-native/recovery-password'

export default RecoveryPassword
