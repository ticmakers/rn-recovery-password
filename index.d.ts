import RecoveryPassword, {
  TypeRecoveryPasswordSource,
  TypeRecoveryPasswordLang,
  IRecoveryPasswordModelProps,
  IRecoveryPasswordState,
  IRecoveryPasswordProps,
} from './src/index.d'

declare module '@ticmakers-react-native/recovery-password'

export default RecoveryPassword

export {
  TypeRecoveryPasswordSource,
  TypeRecoveryPasswordLang,
  IRecoveryPasswordModelProps,
  IRecoveryPasswordState,
  IRecoveryPasswordProps,
}
