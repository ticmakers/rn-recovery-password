"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var core_1 = require("@ticmakers-react-native/core");
var flexbox_1 = require("@ticmakers-react-native/flexbox");
var button_1 = require("@ticmakers-react-native/button");
var image_1 = require("@ticmakers-react-native/image");
var input_1 = require("@ticmakers-react-native/input");
var styles_1 = require("./styles");
var RecoveryPassword = (function (_super) {
    __extends(RecoveryPassword, _super);
    function RecoveryPassword(props) {
        var _this = _super.call(this, props) || this;
        _this.labels = {
            en: {
                email: 'Email',
            },
            es: {
                email: 'Correo electrónico',
            },
        };
        _this.state = _this._processProps();
        return _this;
    }
    RecoveryPassword.prototype.render = function () {
        var _a = this._processProps(), containerStyle = _a.containerStyle, layoutSize = _a.layoutSize, shadowBox = _a.shadowBox, useHeader = _a.useHeader;
        var containerProps = {
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.container, shadowBox && styles_1.default.boxShadow, containerStyle]),
            style: react_native_1.StyleSheet.flatten([!useHeader && styles_1.default.wrapper]),
        };
        var logoContainerSize = (layoutSize && typeof layoutSize.logo !== 'undefined' ? layoutSize.logo : 2);
        var topContainerSize = (layoutSize && typeof layoutSize.top !== 'undefined' ? layoutSize.top : 1);
        var formContainerSize = (layoutSize && typeof layoutSize.form !== 'undefined' ? layoutSize.form : 3);
        var actionsContainerSize = (layoutSize && typeof layoutSize.actions !== 'undefined' ? layoutSize.actions : 1);
        var footerContainerSize = (layoutSize && typeof layoutSize.footer !== 'undefined' ? layoutSize.footer : 1);
        return (React.createElement(flexbox_1.Grid, __assign({}, containerProps),
            React.createElement(flexbox_1.Row, { size: logoContainerSize }, this.Logo()),
            React.createElement(flexbox_1.Row, { size: topContainerSize }, this.Top()),
            React.createElement(flexbox_1.Row, { size: formContainerSize }, this.Form()),
            React.createElement(flexbox_1.Row, { size: actionsContainerSize }, this.Actions()),
            React.createElement(flexbox_1.Row, { size: footerContainerSize }, this.Footer())));
    };
    RecoveryPassword.prototype.Logo = function () {
        var _a = this._processProps(), LogoComponent = _a.LogoComponent, hideLogo = _a.hideLogo, logoContainerStyle = _a.logoContainerStyle, logoSize = _a.logoSize, logoSource = _a.logoSource;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.logoContainer, logoContainerStyle]),
        };
        var imageProps = {
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.logoImageContainer]),
            source: logoSource,
            style: react_native_1.StyleSheet.flatten([styles_1.default.logoImage, logoSize && { height: logoSize }]),
        };
        if (!hideLogo) {
            if (core_1.AppHelper.isComponent(LogoComponent)) {
                return (React.createElement(flexbox_1.Col, __assign({}, containerProps, { direction: "row" }), LogoComponent));
            }
            return (React.createElement(flexbox_1.Col, __assign({}, containerProps),
                React.createElement(image_1.default, __assign({}, imageProps))));
        }
    };
    RecoveryPassword.prototype.Top = function () {
        var _a = this._processProps(), TopComponent = _a.TopComponent, hideTop = _a.hideTop, topContainerStyle = _a.topContainerStyle, title = _a.title, titleStyle = _a.titleStyle;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.topContainer, topContainerStyle]),
        };
        var titleProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.topTitle, titleStyle]),
        };
        if (!hideTop) {
            var titleComponent = void 0;
            var topComponent = void 0;
            if (title) {
                if (core_1.AppHelper.isComponent(title) || core_1.AppHelper.isElement(title)) {
                    titleComponent = React.cloneElement(title, titleProps);
                }
                else {
                    titleComponent = React.createElement(react_native_1.Text, __assign({}, titleProps), "title");
                }
            }
            if (TopComponent && (core_1.AppHelper.isComponent(TopComponent) || core_1.AppHelper.isElement(TopComponent))) {
                topComponent = React.cloneElement(TopComponent);
            }
            return (React.createElement(flexbox_1.Col, __assign({}, containerProps),
                titleComponent,
                topComponent));
        }
    };
    RecoveryPassword.prototype.Form = function () {
        var _this = this;
        var _a = this._processProps(), emailProps = _a.emailProps, hideForm = _a.hideForm, hideSubmit = _a.hideSubmit, hideValidationIcons = _a.hideValidationIcons, lang = _a.lang;
        var _lang = lang || 'en';
        var defaultEmailLabel = this.labels[_lang] && this.labels[_lang].email || 'Email';
        var emailIconName = react_native_1.Platform.select({ android: 'email', ios: 'ios-mail' });
        var emailIconType = react_native_1.Platform.select({ android: 'material-community', ios: 'ionicon' });
        var emailIcon = { name: emailIconName, type: emailIconType };
        var inputEmailProps = {
            hideValidationIcons: hideValidationIcons,
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.formInputContainer]),
            iconLeft: (emailProps && emailProps.icon) || emailIcon,
            label: (emailProps && emailProps.label) || defaultEmailLabel,
            lang: _lang,
            rules: (emailProps && emailProps.rules) || { required: true, email: true },
        };
        if (!hideForm) {
            return (React.createElement(flexbox_1.Col, { style: [styles_1.default.formContainer] },
                React.createElement(input_1.default, __assign({ ref: function (c) { return _this.emailInput = c; } }, inputEmailProps)),
                !hideSubmit && this.SubmitButton()));
        }
    };
    RecoveryPassword.prototype.Actions = function () {
        var _a = this._processProps(), ActionsComponent = _a.ActionsComponent, hideActions = _a.hideActions, actionsContainerStyle = _a.actionsContainerStyle;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.actionsContainer, actionsContainerStyle]),
        };
        if (!hideActions) {
            if (ActionsComponent && (core_1.AppHelper.isComponent(ActionsComponent) || core_1.AppHelper.isElement(ActionsComponent))) {
                return React.cloneElement(ActionsComponent, containerProps);
            }
            return React.createElement(flexbox_1.Col, __assign({}, containerProps));
        }
    };
    RecoveryPassword.prototype.SubmitButton = function () {
        var _this = this;
        var _a = this._processProps(), SubmitComponent = _a.SubmitComponent, hideSubmit = _a.hideSubmit, lang = _a.lang, submitLabel = _a.submitLabel, submitStyle = _a.submitStyle;
        var _lang = lang || 'en';
        var buttonProps = {
            block: true,
            onPress: function () { return _this._onSubmit(); },
            style: react_native_1.StyleSheet.flatten([styles_1.default.btn, styles_1.default.formSubmit, submitStyle]),
            title: submitLabel ? submitLabel : _lang === 'es' ? 'Enviar' : 'Send',
        };
        if (!hideSubmit) {
            if (core_1.AppHelper.isComponent(SubmitComponent)) {
                return React.cloneElement(SubmitComponent);
            }
            return React.createElement(button_1.default, __assign({}, buttonProps));
        }
    };
    RecoveryPassword.prototype.Footer = function () {
        var _this = this;
        var _a = this._processProps(), FooterComponent = _a.FooterComponent, hideFooter = _a.hideFooter, lang = _a.lang;
        var _lang = lang || 'en';
        var footerButtonProps = {
            center: true,
            link: true,
            onPress: function () { return _this._onBack(); },
            title: _lang === 'es' ? 'Volver' : 'Back',
        };
        if (!hideFooter) {
            if (core_1.AppHelper.isComponent(FooterComponent)) {
                return (React.createElement(flexbox_1.Col, { style: [styles_1.default.footerContainer], direction: "row" }, FooterComponent));
            }
            return (React.createElement(flexbox_1.Col, { style: [styles_1.default.footerContainer], direction: "row" },
                React.createElement(button_1.default, __assign({}, footerButtonProps))));
        }
    };
    RecoveryPassword.prototype.isValid = function () {
        var emailValue = this.emailInput && this.emailInput.state.value;
        var emailIsValid = this.emailInput && this.emailInput.isValid();
        return (!!emailValue && !!emailIsValid);
    };
    RecoveryPassword.prototype._onSubmit = function () {
        var onSubmit = this._processProps().onSubmit;
        if (onSubmit) {
            return onSubmit();
        }
    };
    RecoveryPassword.prototype._onBack = function () {
        var onBack = this._processProps().onBack;
        if (onBack) {
            return onBack();
        }
    };
    RecoveryPassword.prototype._processProps = function () {
        var _a = this.props, ActionsComponent = _a.ActionsComponent, FooterComponent = _a.FooterComponent, LogoComponent = _a.LogoComponent, SubmitComponent = _a.SubmitComponent, TopComponent = _a.TopComponent, actionsContainerStyle = _a.actionsContainerStyle, containerStyle = _a.containerStyle, emailProps = _a.emailProps, hideActions = _a.hideActions, hideFooter = _a.hideFooter, hideForm = _a.hideForm, hideLogo = _a.hideLogo, hideSubmit = _a.hideSubmit, hideTop = _a.hideTop, hideValidationIcons = _a.hideValidationIcons, lang = _a.lang, layoutSize = _a.layoutSize, logoContainerStyle = _a.logoContainerStyle, logoSize = _a.logoSize, logoSource = _a.logoSource, onBack = _a.onBack, onSubmit = _a.onSubmit, shadowBox = _a.shadowBox, submitLabel = _a.submitLabel, submitStyle = _a.submitStyle, title = _a.title, titleStyle = _a.titleStyle, topContainerStyle = _a.topContainerStyle, useHeader = _a.useHeader;
        var props = {
            ActionsComponent: (typeof ActionsComponent !== 'undefined' ? ActionsComponent : undefined),
            FooterComponent: (typeof FooterComponent !== 'undefined' ? FooterComponent : undefined),
            LogoComponent: (typeof LogoComponent !== 'undefined' ? LogoComponent : undefined),
            SubmitComponent: (typeof SubmitComponent !== 'undefined' ? SubmitComponent : undefined),
            TopComponent: (typeof TopComponent !== 'undefined' ? TopComponent : undefined),
            actionsContainerStyle: (typeof actionsContainerStyle !== 'undefined' ? actionsContainerStyle : undefined),
            containerStyle: (typeof containerStyle !== 'undefined' ? containerStyle : undefined),
            emailProps: (typeof emailProps !== 'undefined' ? emailProps : undefined),
            hideActions: (typeof hideActions !== 'undefined' ? hideActions : false),
            hideFooter: (typeof hideFooter !== 'undefined' ? hideFooter : false),
            hideForm: (typeof hideForm !== 'undefined' ? hideForm : false),
            hideLogo: (typeof hideLogo !== 'undefined' ? hideLogo : false),
            hideSubmit: (typeof hideSubmit !== 'undefined' ? hideSubmit : false),
            hideTop: (typeof hideTop !== 'undefined' ? hideTop : false),
            hideValidationIcons: (typeof hideValidationIcons !== 'undefined' ? hideValidationIcons : false),
            lang: (typeof lang !== 'undefined' ? lang : 'en'),
            layoutSize: (typeof layoutSize !== 'undefined' ? layoutSize : undefined),
            logoContainerStyle: (typeof logoContainerStyle !== 'undefined' ? logoContainerStyle : undefined),
            logoSize: (typeof logoSize !== 'undefined' ? logoSize : undefined),
            logoSource: (typeof logoSource !== 'undefined' ? logoSource : require('./../assets/icon.png')),
            onBack: (typeof onBack !== 'undefined' ? onBack : undefined),
            onSubmit: (typeof onSubmit !== 'undefined' ? onSubmit : undefined),
            shadowBox: (typeof shadowBox !== 'undefined' ? shadowBox : false),
            submitLabel: (typeof submitLabel !== 'undefined' ? submitLabel : undefined),
            submitStyle: (typeof submitStyle !== 'undefined' ? submitStyle : undefined),
            title: (typeof title !== 'undefined' ? title : undefined),
            titleStyle: (typeof titleStyle !== 'undefined' ? titleStyle : undefined),
            topContainerStyle: (typeof topContainerStyle !== 'undefined' ? topContainerStyle : undefined),
            useHeader: (typeof useHeader !== 'undefined' ? useHeader : false),
        };
        return props;
    };
    return RecoveryPassword;
}(React.Component));
exports.default = RecoveryPassword;
//# sourceMappingURL=RecoveryPassword.js.map